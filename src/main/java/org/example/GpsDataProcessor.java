package org.example;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;

public class GpsDataProcessor {

    public static void main(String[] args) {
        // Пример данных в формате HEX
        String hexData = "539e598f8a020003020700e378351ac39f040c04ffa92e806a28a13b1f00b638030c000067052c06ffffffff033006808001180003200100000700e478351ac40204303dab2e80cb27a13b1c00ac40021b30e778351ac502043082a72e8054020530bf30021b30e978351ac69f0d0c0433a72e80c123a13b2000df3807002579351ac79f06020a170000df28021b476179351ac8020f30021c81279d79351ac9020f30021c8207d979351aca020f30021c8157157a351acb022b8140517a351acc022b608d7a351acd022b60c97a351ace022b30057b351acf022b30417b351ad0022b307d7b351ad1020f3048021b30b97b351ad2020f3070030c004066021630f57b351ad30213841080021730317c351ad4021330c00217306d7c351ad5020f3050021b30a97c351ad602138170021830e57c351ad7020f3058021b30217d351ad8021385500218305d7d351ad9022b8110997d351ada022b8170d57d351adb022b30117e351adc020f3068030ce184680216304d7e351add020f3060030ce34469021630897e351ade021260e4021830c57e351adf021230e5021830017f351ae002293022f2";

        // Преобразование HEX в байтовый массив
        byte[] hexBytes = hexStringToByteArray(hexData);

        // Запись данных в файл Excel
        writeDataToExcel(hexBytes, "gps_data.xlsx");
    }

    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    private static void writeDataToExcel(byte[] data, String filePath) {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("GPS Data");
            int rowIndex = 0;

            // Заголовок таблицы
            Row headerRow = sheet.createRow(rowIndex++);
            headerRow.createCell(0).setCellValue("Index");
            headerRow.createCell(1).setCellValue("Value");

            // Данные
            for (int i = 0; i < data.length; i++) {
                Row dataRow = sheet.createRow(rowIndex++);
                dataRow.createCell(0).setCellValue(i);
                dataRow.createCell(1).setCellValue(Byte.toUnsignedInt(data[i]));
            }

            // Сохранение в файл
            try (FileOutputStream fileOut = new FileOutputStream(filePath)) {
                workbook.write(fileOut);
                System.out.println("Данные успешно записаны в файл Excel: " + filePath);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}